﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Weapon
{
    public class Gun : AbstractWeapon
    {
        private int _capacity;
        private double _calibre;
        public int Capacity
        {
            get { return _capacity; }
            set { _capacity = value; }
        }
        public double Calibre
        {
            get { return _calibre; }
            set { _calibre = value; }
        }

    }
}
