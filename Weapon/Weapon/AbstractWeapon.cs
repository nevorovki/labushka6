﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Weapon
{
   public class AbstractWeapon
    {
        private string _name;
        private int _price;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public int Price
        {
            get { return _price; }
            set { _price = value; }
        }
    }
}
