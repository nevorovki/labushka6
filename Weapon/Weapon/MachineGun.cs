﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Weapon
{
    public class MachineGun : Gun 
    {
        private int _rapidity;

        public int Rapidity
        {
            get { return _rapidity; }
            set { _rapidity = value; }
        }

    }
}
