﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Weapon
{
    public class SniperRifle : Gun
    {
        private int _accuracy;

        public int Accuracy
        {
            get { return _accuracy; }
            set { _accuracy = value; }
        }

    }
}
