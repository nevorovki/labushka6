﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Weapon
{
     public class Division
    {
         private List<AbstractWeapon> weapons;

         public Division(){
             if (weapons == null)
             {
                 weapons = new List<AbstractWeapon>();
             }
         }

         public void AddWeapon(AbstractWeapon weapon) { 
         weapons.Add(weapon);
        
         }

         public List<AbstractWeapon> Weapons
         {
             get { return weapons; }
         }
    }
}
