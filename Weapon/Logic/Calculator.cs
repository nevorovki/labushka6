﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Weapon;

namespace Logic
{
    public class Calculator
    {
        public int GetPrice(Division division) 
          { 
            int totalPrice=0;
            List<AbstractWeapon> weapons = division.Weapons;

            foreach (AbstractWeapon weapon in weapons)
            {
                totalPrice += weapon.Price * 8;
            }

            return totalPrice; 
            } 
    }
}
