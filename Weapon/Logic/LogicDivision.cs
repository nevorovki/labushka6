﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Weapon;


namespace Logic
{
    public class DivisionFactory
    {
        public static Division CreateDivision()
        {
            Gun gun = new Gun();
            gun.Name = "ТТ";
            gun.Price = 10000;
            gun.Calibre = 5.45;
            gun.Capacity = 8;

            Blade blade = new Blade();
            blade.Name = "Штык-нож";
            blade.Price = 1500;
            blade.Lenght = 20;

            MachineGun machinegun = new MachineGun();
            machinegun.Name = "HK21";
            machinegun.Price = 140000;
            machinegun.Rapidity = 9;

            SniperRifle sniperrifle = new SniperRifle();
            sniperrifle.Name = "CBLK-14C";
            sniperrifle.Price = 16000;
            sniperrifle.Accuracy = 7;
            

            Division division = new Division();
            division.AddWeapon(sniperrifle);
            division.AddWeapon(blade);
            division.AddWeapon(gun);
            division.AddWeapon(machinegun);

            return division;
        }
    }
}
    

