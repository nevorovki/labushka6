﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Weapon;
using Logic;

namespace WeaponPrinter
{
    class Printer
    {
        static void Main(string[] args)
        {

            Division division = DivisionFactory.CreateDivision();

            WeaponPrinter printer = new WeaponPrinter();
            printer.Print(division); 

            Calculator calculator = new Calculator();
            int totalPrice = calculator.GetPrice(division);

            Console.WriteLine("Price = " + totalPrice);

            Console.ReadKey(); 
        }
    }
}
